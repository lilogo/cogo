package vip.manda.framework.task;

import vip.manda.framework.core.Import;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author hongda.li 2022-04-26 11:14
 */
@Import({JdkTask.class, CronTask.class})
@Retention(RetentionPolicy.RUNTIME)
@Target(value = {ElementType.TYPE})
public @interface EnableTask {
}
