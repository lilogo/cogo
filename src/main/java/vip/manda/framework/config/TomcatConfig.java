package vip.manda.framework.config;

/**
 * @author hongda.li 2022-04-24 11:17
 */
public interface TomcatConfig {
    String SILENT = "web.silent";
    String ACCEPT_COUNT = "web.acceptCount";
    String ACCEPTOR_THREAD_COUNT = "web.acceptorThreadCount";
    String CONNECTION_TIMEOUT = "web.connectionTimeout";
    String KEEP_ALIVE_TIMEOUT = "web.keepAliveTimeout";
    String MAX_CONNECTIONS = "web.maxConnections";
    String MAX_THREADS = "web.maxThreads";
    String MIN_THREADS = "web.minSpareThreads";

    interface Value {
        boolean SILENT = false;
        int ACCEPT_COUNT = 1000;
        int ACCEPTOR_THREAD_COUNT = 1;
        int MIN_THREADS = 20;
        int MAX_THREADS = 800;
        int MAX_CONNECTIONS = 10000;
        int KEEP_ALIVE_TIMEOUT = 30 * 1000;
        int CONNECTION_TIMEOUT = 60 * 1000;
    }
}
