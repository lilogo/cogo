package vip.manda.framework.data.repository;

import cn.hutool.db.Db;
import cn.hutool.db.Entity;
import cn.hutool.db.Page;
import vip.manda.framework.data.Repository;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author hongda.li 2022-04-12 20:42
 */
public interface BaseRepository extends Repository {

    Db getDb();
    boolean insert(Entity entity);
    Long insertAndGetKey(Entity entity);
    boolean delete(Entity entity);
    boolean delete(int id);
    boolean update(Entity entity, Entity where);
    boolean update(Entity entity, int id);
    Map<String, Object> findOne(Entity entity);
    Map<String, Object> findOne(int id);
    List<Map<String, Object>> findAll();
    List<Map<String, Object>> find(Entity entity);
    List<Map<String, Object>> findPage(Entity entity, Page page);
    List<Map<String, Object>> find(String sql, Object... params);
    List<Map<String, Object>> find(Set<String> columns, String sql, Object... params);
}
