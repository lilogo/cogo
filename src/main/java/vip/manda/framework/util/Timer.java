package vip.manda.framework.util;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUnit;

import java.util.Date;

/**
 * @author hongda.li 2022-04-21 21:27
 */
public class Timer {
    private long startTime;

    public Timer() {
    }

    public Timer(long startTime) {
        this.startTime = System.currentTimeMillis() - startTime * DateUnit.SECOND.getMillis();
    }

    public void start() {
        this.startTime = System.currentTimeMillis();
    }

    public void restart() {
        start();
    }

    public long getNowTime() {
        return startTime != 0L ? (System.currentTimeMillis() - startTime) / DateUnit.SECOND.getMillis() : 0L;
    }

    public static String getSystemTime() {
        return DatePattern.NORM_DATETIME_MS_FORMAT.format(new Date());
    }
}
