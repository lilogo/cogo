package vip.manda.framework.data;

/**
 * @author hongda.li 2022-04-18 21:06
 */
public enum DataType {

    INTEGER("INTEGER"),
    REAL("REAL"),
    TEXT("TEXT");

    private final String type;

    DataType(String type){
        this.type = type;
    }

    public String getType(){
        return this.type;
    }
}
