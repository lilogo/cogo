package vip.manda.framework.log;

import cn.hutool.core.date.DateUtil;
import vip.manda.framework.core.Configs;


/**
 * @author hongda.li 2022-04-12 09:08
 */
public interface LogAppender {

    String ERROR_PATH = Configs.getStr(Configs.Log.ERROR) + DateUtil.today() + ".txt";
    String DEBUG_PATH = Configs.getStr(Configs.Log.DEBUG) + DateUtil.today() + ".txt";

    /**
     * debug的日志记录方法
     * @param message 记录信息
     */
    void debug(String message);

    /**
     * error的日志记录方法
     * @param message 记录信息
     */
    void error(String message);

    /**
     * 判断日志文件的日期是否为当天
     * @param file 日志文件
     * @return 判断结果
     */
    static boolean isTimeOut(String file) {
        return !file.endsWith(DateUtil.today() + ".txt");
    }
}
