package vip.manda.framework.config;

/**
 * @author hongda.li 2022-04-24 14:07
 */
public interface WebConfig {
    String PORT = "web.port";
    String CONTEXT = "web.context";
    String ROOT = "web.root";
    interface Value{
        int PORT = 8090;
        String CONTEXT = "";
        String ROOT = "classpath://";
    }
}
