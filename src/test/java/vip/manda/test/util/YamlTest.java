package vip.manda.test.util;

import vip.manda.framework.config.YamlProp;

/**
 * @author hongda.li 2022-04-26 15:33
 */
public class YamlTest {
    public static void main(String[] args) {
        StringBuilder name = new StringBuilder("spring.application");
        for (int i = 0; i < 20; i++) {
            name.append(".test").append((i+1));
        }
        YamlProp yamlProp = new YamlProp("test.yaml");
        System.out.println(yamlProp.get(name.toString()));
    }
}
