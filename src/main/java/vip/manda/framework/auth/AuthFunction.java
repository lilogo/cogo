package vip.manda.framework.auth;

import vip.manda.framework.bean.BeanFactory;
import vip.manda.framework.core.Configs;
import vip.manda.framework.core.Function;
import vip.manda.framework.log.Log;
import vip.manda.framework.log.LogFactory;

import java.util.Set;

/**
 * @author hongda.li 2022-04-13 17:23
 */
@Function(order = 3)
public class AuthFunction implements Function.FunctionMethod {
    private static final Log log = LogFactory.getLog();

    @Override
    public void doFunction(Set<Class<?>> classes) {
        Configs.setDefaultConfig(Configs.Auth.ENABLE_AUTH, Configs.Auth.Value.ENABLE_AUTH);
        boolean enable = Configs.getBool(Configs.Auth.ENABLE_AUTH);
        if (!enable){
            BeanFactory.getContainer().remove(AuthRepository.class);
            return;
        }
        log.func("execute function[{}]", AuthFunction.class.getName());
        Configs.setDefaultConfig(Configs.Auth.TOKEN_TIME, Configs.Auth.Value.TOKEN_TIME);
        Configs.setDefaultConfig(Configs.Auth.COOKIE_TIME, Configs.Auth.Value.COOKIE_TIME);
    }
}
