package vip.manda.framework.web.http;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author hongda.li 2022-04-11 17:00
 */
public class CrosFilter implements Filter {

    @Override
    public Integer getOrder() {
        return Integer.MIN_VALUE + 20;
    }

    @Override
    public boolean doFilter(HttpServletRequest request, HttpServletResponse response) {
        response.setHeader("Access-Control-Allow-Origin", request.getHeader("Origin"));
        response.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
        response.setHeader("Access-Control-Max-Age", "3600");
        response.setHeader("Access-Control-Allow-Headers", "x-requested-with, Content-Type, Accept, Origin, access_token");
        response.setHeader("Access-Control-Allow-Credentials", "true");
        return true;
    }
}


