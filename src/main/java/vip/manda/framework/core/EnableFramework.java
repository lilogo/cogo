package vip.manda.framework.core;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author hongda.li 2022-04-23 23:04
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(value = {ElementType.TYPE})
@Scanner(EnableFramework.Common.FRAMEWORK_PACKAGE)
public @interface EnableFramework {
    interface Common{
        String FRAMEWORK_PACKAGE = "vip.manda.framework";
    }
}
