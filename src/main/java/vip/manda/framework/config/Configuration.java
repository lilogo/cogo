package vip.manda.framework.config;

import cn.hutool.setting.dialect.Props;

import java.util.Map;

/**
 * @author hongda.li 2022-04-26 15:49
 */
public class Configuration {
    private final Props prop;
    private final YamlProp yamlProp;
    private final Map<String, Object> mapProp;

    public Configuration(Props props){
        this.prop = props;
        this.yamlProp = null;
        this.mapProp = null;
    }

    public Configuration(YamlProp yamlProp){
        this.yamlProp = yamlProp;
        this.prop = null;
        this.mapProp = null;
    }

    public Configuration(Map<String, Object> mapProp){
        this.mapProp = mapProp;
        this.yamlProp = null;
        this.prop = null;
    }
}
