package vip.manda.framework.task;

import cn.hutool.cron.CronUtil;
import vip.manda.framework.log.Log;
import vip.manda.framework.log.LogFactory;

/**
 * @author hongda.li 2022-04-21 15:46
 */
public class CronTask {

    private static final Log log = LogFactory.getLog();

    public String doTask(String cron, Runnable task){
        return CronUtil.schedule(cron, task);
    }

    public void stopTask(String id){
        CronUtil.remove(id);
    }

    public void stopAll(){
        CronUtil.stop();
    }

    public void restart(){
        CronUtil.restart();
    }
}
