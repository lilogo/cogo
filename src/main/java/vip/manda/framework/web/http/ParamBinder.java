package vip.manda.framework.web.http;

import cn.hutool.core.util.ReflectUtil;
import vip.manda.framework.bean.BeanFactory;
import vip.manda.framework.util.Triad;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.ArrayList;
import java.util.List;

/**
 * @author hongda.li
 */
public class ParamBinder {

    private final Triad<String, Class<?>, Method> controller;

    private final HttpServletRequest request;

    private final HttpServletResponse response;

    public ParamBinder(Triad<String, Class<?>, Method> controller, HttpServletRequest request, HttpServletResponse response) {
        this.controller = controller;
        this.request = request;
        this.response = response;
    }

    public Object invoke() throws InvocationTargetException, IllegalAccessException {
        Object instance = BeanFactory.getContainer().getBean(controller.getY());
        Parameter[] parameters = controller.getZ().getParameters();
        List<String> parameterNames = getParameterNames();
        List<Object> parameterValues = new ArrayList<>();
        if (parameters != null && parameters.length > 0) {
            for (int i = 0; i < parameters.length; i++) {
                if (parameters[i].getType() == HttpServletRequest.class) {
                    parameterValues.add(request);
                } else if (parameters[i].getType() == HttpServletResponse.class) {
                    parameterValues.add(response);
                } else if (parameters[i].getType() == HttpSession.class) {
                    parameterValues.add(request.getSession());
                } else if (parameters[i].getType() == Integer.class || parameters[i].getType() == int.class) {
                    parameterValues.add(Integer.parseInt(request.getParameter(parameterNames.get(i))));
                } else if (parameters[i].getType() == Double.class || parameters[i].getType() == double.class) {
                    parameterValues.add(Double.parseDouble(request.getParameter(parameterNames.get(i))));
                } else if (parameters[i].getType() == Long.class || parameters[i].getType() == long.class) {
                    parameterValues.add(Long.parseLong(request.getParameter(parameterNames.get(i))));
                } else if (parameters[i].getType() == Boolean.class || parameters[i].getType() == boolean.class) {
                    parameterValues.add(Boolean.parseBoolean(request.getParameter(parameterNames.get(i))));
                } else if (parameters[i].getType() == String.class) {
                    parameterValues.add(request.getParameter(parameterNames.get(i)));
                } else {
                    Class<?> beanClass = parameters[i].getType();
                    parameterValues.add(getBean(beanClass));
                }
            }
        }
        return ReflectUtil.invoke(instance, controller.getZ(), parameterValues.toArray());
    }


    public Object getBean(Class<?> tClass) {
        try {
            Object instance = ReflectUtil.newInstance(tClass);
            Field[] fields = ReflectUtil.getFields(tClass);
            for (Field field : fields) {
                String parameter = request.getParameter(field.getName());
                if (null != parameter) {
                    Method setMethod;
                    if (!field.getName().startsWith("is")) {
                        setMethod = ReflectUtil.getMethodByNameIgnoreCase(tClass, "set" + field.getName());
                    } else {
                        setMethod = ReflectUtil.getMethodByNameIgnoreCase(tClass, "set" + field.getName().substring(2));
                    }
                    ReflectUtil.invoke(instance, setMethod, parameter);
                }
            }
            return instance;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 获取方法的参数名称
     *
     * @return 参数名称集合
     */
    private List<String> getParameterNames() {
        List<String> parameterNames = new ArrayList<>();
        for (Parameter parameter : controller.getZ().getParameters()) {
            parameterNames.add(parameter.getName());
        }
        return parameterNames;
    }
}
