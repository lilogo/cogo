package vip.manda.framework.log;

import cn.hutool.core.date.DateUtil;

import java.util.List;

/**
 * @author hongda.li 2022-04-11 12:46
 */
public class Log{

    private final String loggerName;
    private final List<LogAppender> appenderList;

    public Log(String logger){
        this.loggerName = logger;
        this.appenderList = LogFunction.getAppender();
    }

    public void info(String template, Object... values){
        LogPrinter.info(loggerName, template, values);
    }

    public void func(String template, Object... values){
        LogPrinter.func(loggerName, template, values);
    }

    public void debug(String template, Object... values){
        LogPrinter.debug(loggerName, template, values);
        appenderList.forEach(appender -> appender.debug(String.format(LogFactory.TEMPLATE
                , DateUtil.now(), loggerName, "DEBUG"
                , LogPrinter.getMessage(template, values))));
    }

    public void error(String template, Object... values){
        LogPrinter.error(loggerName, template, values);
        appenderList.forEach(appender -> appender.error(String.format(LogFactory.TEMPLATE
                , DateUtil.now(), loggerName, "ERROR"
                , LogPrinter.getMessage(template, values))));
    }
}
