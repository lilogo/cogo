package vip.manda.framework.config;

import cn.hutool.core.io.resource.ResourceUtil;
import org.yaml.snakeyaml.Yaml;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author hongda.li 2022-04-26 15:50
 */
public class YamlProp {
    private Yaml yaml;
    private LinkedHashMap<String, Object> params;
    private final Map<String, Object> props = new HashMap<>();

    public YamlProp(){}

    public YamlProp(String yaml){
        this.params = new Yaml().loadAs(ResourceUtil.getStream(yaml), LinkedHashMap.class);
    }

    public Object get(String key) {
        if (this.props.containsKey(key)){
            return this.props.get(key);
        }
        if (!key.contains(".")){
            return this.params.get(key);
        }
        String[] keys = key.split("\\.");
        int count = keys.length;
        Map<String, Object> temp = params;
        for (int i = 0; i < count; i++) {
            if (i == count - 1){
                Object value = temp.get(keys[i]);
                this.props.put(key, value);
                return value;
            }
            temp = (Map<String, Object>) temp.get(keys[i]);
        }
        return null;
    }

    public void put(String key, Object value){
        this.props.put(key, value);
    }
}
