package vip.manda.framework.web;

/**
 * @author hongda.li 2022-04-26 14:31
 * 请求控制器
 * 包含Api请求路径、控制器类、控制器实例以及Api对应的方法
 */
public class Controller {
    private String api;
    private Class<?> clazz;
    private Object instance;
    private Method method;

    public String getApi() {
        return api;
    }

    public void setApi(String api) {
        this.api = api;
    }

    public Class<?> getClazz() {
        return clazz;
    }

    public void setClazz(Class<?> clazz) {
        this.clazz = clazz;
    }

    public Object getInstance() {
        return instance;
    }

    public void setInstance(Object instance) {
        this.instance = instance;
    }

    public Method getMethod() {
        return method;
    }

    public void setMethod(Method method) {
        this.method = method;
    }
}
