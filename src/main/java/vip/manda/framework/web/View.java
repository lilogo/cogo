package vip.manda.framework.web;

import cn.hutool.extra.template.TemplateConfig;
import cn.hutool.extra.template.TemplateEngine;
import cn.hutool.extra.template.TemplateUtil;
import vip.manda.framework.config.WebConfig;
import vip.manda.framework.core.Configs;

import java.nio.charset.StandardCharsets;
import java.util.Map;

/**
 * @author hongda.li 2022-04-17 22:23
 */
public class View {

    private String name;
    private Map<String, Object> params;

    public static final TemplateEngine ENGINE;

    static {
        TemplateConfig templateConfig = new TemplateConfig();
        templateConfig.setCharset(StandardCharsets.UTF_8);
        templateConfig.setResourceMode(TemplateConfig.ResourceMode.CLASSPATH);
        String root = Configs.getStr(WebConfig.ROOT);
        templateConfig.setPath(root.equals(WebConfig.Value.ROOT) ? "" : root.substring(12));
        ENGINE = TemplateUtil.createEngine(templateConfig);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map<String, Object> getParams() {
        return params;
    }

    public void setParams(Map<String, Object> params) {
        this.params = params;
    }

    public static View of(String name, Map<String, Object> params){
        View view = new View();
        view.setName(name);
        view.setParams(params);
        return view;
    }
}
