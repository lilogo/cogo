package vip.manda.test.mybatis;

/**
 * @author hongda.li 2022-04-26 10:43
 */
public class Demo {
    private int id;
    private String name;
    private String description;

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
}
