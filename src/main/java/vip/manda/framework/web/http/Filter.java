package vip.manda.framework.web.http;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author hongda.li 2022-04-11 17:21
 */
public interface Filter {

    interface Common{
        String GET_ORDER = "getOrder";
        String DO_FILTER = "doFilter";
    }

    /**
     * 获取优先级顺序
     * @return 优先级顺序
     */
    Integer getOrder();

    /**
     * 执行过滤逻辑
     * @param request 请求
     * @param response 响应
     * @return 是否放行
     * @throws IOException IO异常
     */
    boolean doFilter(HttpServletRequest request, HttpServletResponse response) throws IOException;
}
