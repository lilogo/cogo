package vip.manda.framework.web;

/**
 * @author hongda.li 2022/3/15 20:18
 */
public enum Method {

    /**
     * GET方法
     */
    GET("GET"),

    /**
     * POST方法
     */
    POST("POST"),

    /**
     * DELETE方法
     */
    DELETE("DELETE"),

    /**
     * PUT方法
     */
    PUT("PUT");

    private final String value;

    Method(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }
}
