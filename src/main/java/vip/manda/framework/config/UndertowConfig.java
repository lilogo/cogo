package vip.manda.framework.config;

import cn.hutool.core.net.NetUtil;

/**
 * @author hongda.li 2022-04-24 11:17
 */
public interface UndertowConfig {
    String HOST = "undertow.hostname";
    String THREAD_IO = "undertow.thread.io";
    String THREAD_WORK = "undertow.thread.work";
    String BUFFER_SIZE = "undertow.buffer.size";
    String DIRECT_BUFFERS = "undertow.direct-buffers";
    interface Values{
        String HOST = NetUtil.getLocalhostStr();
        int THREAD_IO = 4;
        int THREAD_WORK = 128;
        int BUFFER_SIZE = 1024;
        boolean DIRECT_BUFFERS = true;
    }
}
