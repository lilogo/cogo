package vip.manda.test.annotation;

import cn.hutool.core.annotation.AnnotationUtil;
import cn.hutool.core.annotation.CombinationAnnotationElement;
import vip.manda.framework.core.Application;
import vip.manda.framework.core.Scanner;

import java.lang.annotation.Annotation;

/**
 * @author hongda.li 2022-04-25 14:32
 */
@Test2
@Test1
@Test3
public class Test {
    public static void main(String[] args) {
        for (Scanner annotation : Application.getAnnotations(Test.class, Scanner.class)) {
            System.out.println(annotation.value()[0]);
        }
    }
}
