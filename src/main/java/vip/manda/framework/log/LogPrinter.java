package vip.manda.framework.log;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.ClassUtil;
import cn.hutool.core.util.StrUtil;

import java.util.Date;

/**
 * @author hongda.li 2022/3/15 17:16
 */
public class LogPrinter {
    public static final String FUNC_LEVEL = "FUNC";
    public static final String INFO_LEVEL = "INFO";
    public static final String DEBUG_LEVEL = "DEBUG";
    public static final String ERROR_LEVEL = "ERROR";
    public static final int ERROR = 31;
    public static final int INFO = 32;
    public static final int DEBUG = 33;
    public static final int MESSAGE = 35;
    public static final int CLASSNAME = 36;
    public static final int NORMAL = 37;
    public static final int NONE = 38;
    public static final String LINE_SEPARATOR = System.lineSeparator();
    public static void func(String className, String template, Object... values){
        show(FUNC_LEVEL, className, template, values);
    }

    public static void error(String className, String template, Object... values) {
        show(ERROR_LEVEL, className, template, values);
    }

    public static void debug(String className, String template, Object... values) {
        show(DEBUG_LEVEL, className, template, values);
    }

    public static void info(String className, String template, Object... values) {
        show(INFO_LEVEL, className, template, values);
    }

    private static void show(String level, String className, String template, Object... values) {
        formatLog(level, className, getMessage(template, values));
    }

    public static String getMessage(String template, Object... values) {
        if (! ArrayUtil.isEmpty(values) && ! StrUtil.contains(template, "{}")) {
            return StrUtil.format(buildTemplateSplitBySpace(values.length + 1), ArrayUtil.insert(values, 0, template));
        } else {
            return StrUtil.format(template, values);
        }
    }

    private static synchronized void formatLog(String level, String className, String message) {
        System.out.format("\33[%d;2m%s", NORMAL, DatePattern.NORM_DATETIME_MS_FORMAT.format(new Date()));
        switch (level) {
            case DEBUG_LEVEL:
                System.out.format("\33[%d;2m%-8s", DEBUG, "  DEBUG");
                System.out.format("\33[%d;2m%s", DEBUG, " --- ");
                break;
            case ERROR_LEVEL:
                System.out.format("\33[%d;2m%-8s", ERROR, "  ERROR");
                System.out.format("\33[%d;2m%s", ERROR, " --- ");
                break;
            case INFO_LEVEL:
                System.out.format("\33[%d;2m%-8s", INFO, "  INFO");
                System.out.format("\33[%d;2m%s", INFO, " --- ");
                break;
            case FUNC_LEVEL:
                System.out.format("\33[%d;2m%-8s", MESSAGE, "  FUNC");
                System.out.format("\33[%d;2m%s", MESSAGE, " --- ");
                System.out.format("\33[%d;2m%-35s", CLASSNAME, "[" + ClassUtil.getShortClassName(className) + "]");
                System.out.format("\33[%d;2m%s", MESSAGE, " : " + message);
                System.out.format("%s", LINE_SEPARATOR);
                System.out.format("\33[%d;2m%s", NONE, "");
                return;
            default:
        }
        System.out.format("\33[%d;2m%-35s", CLASSNAME, "[" + ClassUtil.getShortClassName(className) + "]");
        System.out.format("\33[%d;2m%s", NONE, " : " + message);
        System.out.format("%s", LINE_SEPARATOR);
        System.out.format("\33[%d;2m%s", NONE, "");
    }


    private static String buildTemplateSplitBySpace(int count) {
        return StrUtil.repeatAndJoin("{}", count, " ");
    }
}
