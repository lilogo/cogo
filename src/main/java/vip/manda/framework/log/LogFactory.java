package vip.manda.framework.log;

import cn.hutool.core.lang.caller.CallerUtil;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author hongda.li 2022-04-11 12:47
 */
public class LogFactory {
    private static final Map<String, Log> LOG_CACHE = new ConcurrentHashMap<>();
    public static final String TEMPLATE = "%s  %s [%s] : %s\r\n";

    public static Log getLog(){
        Class<?> caller = CallerUtil.getCallerCaller();
        LOG_CACHE.computeIfAbsent(caller.getName(), Log::new);
        return LOG_CACHE.get(caller.getName());
    }

    public static Log getLog(String loggerName){
        LOG_CACHE.computeIfAbsent(loggerName, Log::new);
        return LOG_CACHE.get(loggerName);
    }

    public static Log getLog(Class<?> logger){
        LOG_CACHE.computeIfAbsent(logger.getName(), Log::new);
        return LOG_CACHE.get(logger.getName());
    }
}
