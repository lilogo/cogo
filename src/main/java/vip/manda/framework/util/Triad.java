package vip.manda.framework.util;

import java.util.*;

/**
 * @author hongda.li 2022-04-12 12:34
 */
public class Triad<X, Y, Z> {

    private final X x;
    private final Y y;
    private final Z z;

    public Triad(X x, Y y, Z z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public X getX() {
        return x;
    }

    public Y getY() {
        return y;
    }

    public Z getZ() {
        return z;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Triad<?, ?, ?> triad = (Triad<?, ?, ?>) o;
        return Objects.equals(x, triad.x) && Objects.equals(y, triad.y) && Objects.equals(z, triad.z);
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y, z);
    }

    public static <X, Y, Z> Triad<X, Y, Z> of(X x, Y y, Z z) {
        return new Triad<>(x, y, z);
    }
}
