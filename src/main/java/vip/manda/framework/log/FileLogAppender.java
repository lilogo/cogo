package vip.manda.framework.log;

import cn.hutool.core.io.file.FileWriter;

/**
 * @author hongda.li 2022-04-22 11:54
 */
public class FileLogAppender implements LogAppender{
    private FileWriter debugWriter = new FileWriter(LogAppender.DEBUG_PATH);
    private FileWriter errorWriter = new FileWriter(LogAppender.ERROR_PATH);
    @Override
    public void debug(String message) {
        if (LogAppender.isTimeOut(LogAppender.DEBUG_PATH)){
            this.debugWriter = new FileWriter(LogAppender.DEBUG_PATH);
        }
        debugWriter.append(message);
    }

    @Override
    public void error(String message) {
        if (LogAppender.isTimeOut(LogAppender.ERROR_PATH)){
            this.errorWriter = new FileWriter(LogAppender.ERROR_PATH);
        }
        errorWriter.append(message);
    }
}
