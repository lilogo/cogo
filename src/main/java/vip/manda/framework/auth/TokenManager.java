package vip.manda.framework.auth;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.jwt.JWTUtil;
import cn.hutool.jwt.signers.NoneJWTSigner;
import vip.manda.framework.core.Configs;
import vip.manda.framework.util.Timer;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author hongda.li 2022-04-02 20:09
 */
public class TokenManager {

    private static final String TOKEN_NAME = "application_token";
    private static final String CREATE_TIME = "create_time";
    private static final String EXPIRE_TIME = "expire_time";

    /**
     * 创建token
     * @param message 携带信息
     * @return token值
     */
    public static String createToken(String message){
        Map<String, Object> params = new HashMap<>(2);
        params.put(TOKEN_NAME, message);
        return createToken(params);
    }

    /**
     * 创建token
     * @param params 参数
     * @return token值
     */
    public static String createToken(Map<String, Object> params){
        int tokenTime = Configs.getInt(Configs.Auth.TOKEN_TIME);
        params.put(CREATE_TIME, Timer.getSystemTime());
        params.put(EXPIRE_TIME, tokenTime);
        return JWTUtil.createToken(params, NoneJWTSigner.NONE);
    }

    /**
     * 验证token是否有效
     * @param token token值
     * @return 是否有效
     */
    public static boolean verifyToken(String token){
        if (token == null || StrUtil.isEmpty(token)){
            return false;
        }
        JSONObject payloads = JWTUtil.parseToken(token).getPayloads();
        long expireTime = payloads.get(EXPIRE_TIME, Long.class);
        String start = payloads.get(CREATE_TIME, String.class);
        long between = DateUtil.between(DateUtil.parse(start), DateUtil.parse(Timer.getSystemTime()), DateUnit.MINUTE);
        return between < expireTime;
    }

    /**
     * 获取token的携带信息
     * @param token token值
     * @return 携带信息
     */
    public static String getTokenValue(String token){
        return getTokenValue(token, TOKEN_NAME, String.class);
    }

    /**
     * 获取token参数信息
     * @param token token值
     * @param name 参数名称
     * @param tClass 参数类型
     * @param <T> 参数泛型
     * @return 参数值
     */
    public static <T> T getTokenValue(String token, String name, Class<T> tClass){
        return verifyToken(token) ? JWTUtil.parseToken(token).getPayloads().get(name, tClass) : null;
    }
}
