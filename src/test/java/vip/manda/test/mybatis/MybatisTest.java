package vip.manda.test.mybatis;

import vip.manda.framework.bean.Inject;
import vip.manda.framework.bean.Singleton;
import vip.manda.framework.core.Application;
import vip.manda.framework.core.Configs;
import vip.manda.framework.core.EnableFramework;

/**
 * @author hongda.li 2022-04-26 10:41
 * MyBatis单元测试
 */
@EnableFramework
@Singleton
public class MybatisTest {

    @Inject
    private static DemoMapper mapper;

    public static void main(String[] args) {
        Configs.init();
        // 数据库连接配置
        Configs.rewrite(Configs.DataBase.USERNAME, "hzero");
        Configs.rewrite(Configs.DataBase.PASSWORD, "hzero");
        Configs.rewrite(Configs.DataBase.URL, "jdbc:mysql://localhost:3306/demo?useUnicode=true&characterEncoding=UTF-8&useSSL=false");
        Application.run();

        // 执行查询操作，使用参数绑定
        for (Demo demo : mapper.findAll(1000,1100)) {
            System.out.println(demo.getName());
        }
    }
}
