package vip.manda.test.mybatis;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author hongda.li 2022-04-26 10:42
 */
@Mapper
public interface DemoMapper {

    @Select("select * from demo where id>#{min} and id<#{max}")
    List<Demo> findAll(int min, int max);
}
