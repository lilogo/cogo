package vip.manda.framework.core;

import java.lang.annotation.*;

/**
 * @author hongda.li 2022-04-23 22:59
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(value = {ElementType.TYPE})
@Deprecated
public @interface ConditionOn {
    Class<? extends Annotation> value() default EnableFramework.class;
}
