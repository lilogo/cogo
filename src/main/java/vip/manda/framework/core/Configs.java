package vip.manda.framework.core;

import cn.hutool.setting.dialect.Props;
import vip.manda.framework.log.LogFactory;

import java.util.Arrays;
import java.util.List;

/**
 * @author hongda.li 2022-04-11 14:52
 */
public class Configs {
    private static final String CONFIG_NAME = "application.properties";
    private static final vip.manda.framework.log.Log LOG = LogFactory.getLog();
    private static Props property;
    public static Props getProperty(){
        return property;
    }
    public static void setProperty(String resourceName) {
        property = new Props(resourceName);
    }
    public static void setProperty(Props property) {
        Configs.property = property;
    }
    public static void setDefaultConfig(String name, Object key) {
        if (exists(name)) {
            LOG.info("On configuration[{}={}]", name, property.get(name));
            return;
        }
        LOG.info("On configuration[{}={}]", name, key);
        property.setProperty(name, key);
    }
    public static void init() {
        if (property == null) {
            try {
                property = new Props(CONFIG_NAME);
            } catch (Exception e) {
                // handle none configuration
                property = new Props();
                LOG.debug("None configuration on classpath");
            }
        }
    }
    public static void rewrite(String name, Object value){
        property.setProperty(name, value);
    }

    public static boolean exists(String name) {
        return property.get(name) != null;
    }

    public static int getInt(String name) {
        return property.getInt(name);
    }

    public static long getLong(String name) {
        return property.getLong(name);
    }

    public static String getStr(String name) {
        return property.getStr(name);
    }

    public static boolean getBool(String name) {
        return property.getBool(name);
    }

    public static List<String> getStrList(String name) {
        return Arrays.asList(property.getStr(name).split(";"));
    }

    public interface Log {
        String ERROR = "log.file.error";
        String DEBUG = "log.file.debug";

        interface Value {
            String ERROR = "/storage/log/error/";
            String DEBUG = "/storage/log/debug/";
        }
    }

    public interface DataBase {
        String PATH = "database.path";
        String NAME = "database.name";
        String DRIVER = "database.driver";
        String USERNAME = "database.username";
        String PASSWORD = "database.password";
        String URL = "database.url";

        interface Value {
            String PATH = "/storage/database/";
            String NAME = "application.db";
            String DRIVER = "org.sqlite.JDBC";
            String USERNAME = "root";
            String PASSWORD = "123456";
            String MYSQL_URL = "jdbc:mysql://localhost:3306/application?useUnicode=true&characterEncoding=UTF-8&useSSL=false";
            String SQLITE_URL = "jdbc:sqlite:application";
        }
    }

    public interface Auth {

        String ENABLE_AUTH = "auth.enabled";
        String TOKEN_TIME = "auth.token.time";
        String COOKIE_TIME = "auth.cookie.time";

        interface Value {
            boolean ENABLE_AUTH = false;
            int TOKEN_TIME = 30;
            int COOKIE_TIME = 60 * 60 * 24 * 7;
        }
    }

    public interface Task {
        String THREAD_SIZE = "task.thread.size";

        interface Value {
            int THREAD_SIZE = 16;
        }
    }
}
