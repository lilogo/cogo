package vip.manda.test.mvc.tomcat;

import vip.manda.framework.bean.Singleton;
import vip.manda.framework.core.Application;
import vip.manda.framework.core.EnableFramework;
import vip.manda.framework.core.Remove;
import vip.manda.framework.util.Maps;
import vip.manda.framework.web.Api;
import vip.manda.framework.web.View;
import vip.manda.framework.web.server.UndertowServer;
import vip.manda.test.mybatis.Demo;

import java.util.Arrays;
import java.util.List;

/**
 * @author hongda.li 2022-04-26 10:55
 * Tomcat测试
 */
@EnableFramework
@Singleton
@Remove(UndertowServer.class)
public class TomcatTest {
    public static void main(String[] args) {
        Application.run();
    }

    @Api("/test")
    public String hello(String name){
        return "<h1>hello : " + name + "</h1>";
    }

    @Api("/get")
    public List<Demo> jsonTest(){
        Demo demo1 = new Demo();
        demo1.setId(1);
        demo1.setName("1");
        Demo demo2 = new Demo();
        demo2.setId(2);
        demo2.setName("2");
        return Arrays.asList(demo1, demo2);
    }

    @Api("/view")
    public View render(String name){
        return View.of("test/test.ftl", Maps.of("user", name));
    }
}
