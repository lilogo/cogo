package vip.manda.framework.core;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.List;
import java.util.Set;

/**
 * @author hongda.li 2022-04-11 09:05
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(value = {ElementType.TYPE})
public @interface Function {

    interface Common{
        String ORDER = "order";
        String METHOD = "doFunction";
        int BEAN_FUNCTION = 1;
        int LOG_FUNCTION = Integer.MIN_VALUE + 10;
        int DATA_FUNCTION = 10;
        int WEB_FUNCTION = 100;
        int TASK_FUNCTION = BEAN_FUNCTION - 100;
    }

    interface FunctionMethod{
        /**
         * 执行函数
         * @param classes 已导入的class集合
         */
        void doFunction(Set<Class<?>> classes);
    }

    // 执行优先级
    int order() default Common.WEB_FUNCTION - 10;
    // 执行方法名
    String value() default "doFunction";
}
