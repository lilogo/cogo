# Cogo框架：cogo-framework

## 一、cogo-framework
- 一个基于`Hutool`开源工具库的轻量级Web后端开发框架
- 外部组件依赖且仅依赖于`Hutool`，无`Spring`生态
- 参考`MVC`、`ORM`、`IOC`等众多主流框架的设计思想
- 全注解支持、自定义组件支持
- 提供全套微服务开发组件，如注册中心`[cogo-with-register]`等


## 二、获取最新代码
- 你可以从gitee获取最新代码
- gitee地址：[https://gitee.com/yixi-dlmu/cogo](https://gitee.com/yixi-dlmu/cogo)
- 生态组件地址：[https://gitee.com/yixi-dlmu](https://gitee.com/yixi-dlmu)

## 三、引入方式
- 将工程下载或克隆到本地
- 使用`maven`生命周期提供的`install`插件将`jar`包安装到本地仓库
- 在`maven`工程中引入依赖

## 四、Maven地址
```xml
<dependencies>
    <dependency>
        <groupId>vip.manda</groupId>
        <artifactId>cogo-framework</artifactId>
        <version>{latest.version}</version>
    </dependency>
</dependencies>

<distributionManagement>
    <repository>
        <id>local-maven</id>
        <url>file:${project.basedir}/release</url>
    </repository>
</distributionManagement>
```