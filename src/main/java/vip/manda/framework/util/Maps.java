package vip.manda.framework.util;

import cn.hutool.core.lang.Assert;

import java.util.HashMap;
import java.util.Map;

/**
 * @author hongda.li 2022-04-21 21:10
 */
public class Maps {

    private Maps() {
    }

    public static <K, V> Map<K, V> of(Object... elements) {
        Assert.isTrue(elements.length % 2 == 0);
        Map<K, V> map = new HashMap<>(size(elements.length / 2));
        for (int i = 0; i < elements.length; i+=2) {
            map.put((K) elements[i], (V) elements[i+1]);
        }
        return map;
    }

    public static int size(int size) {
        return (int) ((size / 0.75) + 1);
    }
}
