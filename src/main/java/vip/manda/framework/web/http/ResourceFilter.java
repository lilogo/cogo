package vip.manda.framework.web.http;

import vip.manda.framework.log.Log;
import vip.manda.framework.log.LogFactory;
import vip.manda.framework.util.Requester;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author hongda.li 2022-04-12 11:36
 */
public class ResourceFilter implements Filter {

    private static final Log log = LogFactory.getLog();

    @Override
    public Integer getOrder() {
        return Integer.MIN_VALUE + 10;
    }

    @Override
    public boolean doFilter(HttpServletRequest request, HttpServletResponse response) throws IOException {
        StaticResourceHandler resourceHandler = new StaticResourceHandler(request, response);
        boolean check = resourceHandler.checkStaticResource();
        if (check) {
            response.setContentType(StaticResourceHandler.CONTENT_TYPES.get(".html"));
            resourceHandler.handle();
            log.info("Capture static resource[{}]", Requester.getUri(request.getRequestURI()));
            return false;
        }
        return true;
    }
}
