package vip.manda.framework.log;

import cn.hutool.core.util.ReflectUtil;
import vip.manda.framework.core.Configs;
import vip.manda.framework.core.Function;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author hongda.li 2022-04-12 09:12
 */
@Function(order = Function.Common.LOG_FUNCTION)
public class LogFunction implements Function.FunctionMethod {
    private static final Log log = LogFactory.getLog();
    private static final List<LogAppender> APPENDER_LIST = new ArrayList<>();

    public static List<LogAppender> getAppender() {
        return APPENDER_LIST;
    }

    @Override
    public void doFunction(Set<Class<?>> classes) {
        if (!classes.contains(FileLogAppender.class)){
            return;
        }
        log.func("execute function[{}]", LogFunction.class.getName());
        Configs.setDefaultConfig(Configs.Log.DEBUG, Configs.Log.Value.DEBUG);
        Configs.setDefaultConfig(Configs.Log.ERROR, Configs.Log.Value.ERROR);

        // 获取LogAppender的所有实现类
        classes.stream()
                .filter(LogAppender.class::isAssignableFrom)
                .forEach(appender -> APPENDER_LIST.add((LogAppender) ReflectUtil.newInstance(appender)));
        APPENDER_LIST.forEach(appender -> log.info("Capture a logAppender[{}]", appender.getClass().getName()));
    }
}
