package vip.manda.test.task;

import vip.manda.framework.bean.Inject;
import vip.manda.framework.bean.Singleton;
import vip.manda.framework.core.Application;
import vip.manda.framework.core.EnableFramework;
import vip.manda.framework.log.Log;
import vip.manda.framework.log.LogFactory;
import vip.manda.framework.task.CronTask;
import vip.manda.framework.task.EnableTask;
import vip.manda.framework.task.JdkTask;
import vip.manda.framework.util.Timer;

/**
 * @author hongda.li 2022-04-26 11:11
 * 定时任务测试
 */
@Singleton
@EnableFramework
@EnableTask
public class TaskTest {
    @Inject
    private static JdkTask jdkTask;
    @Inject
    private static CronTask cronTask;

    public static void main(String[] args) {
        Application.run();
        // 计时器
        Timer timer = new Timer();
        timer.start();

        // 日志记录
        Log jdkLog = LogFactory.getLog(JdkTask.class);
        Log cronLog = LogFactory.getLog(CronTask.class);

        // 每两秒执行一次
        jdkTask.doBySeconds(() -> jdkLog.debug("I am jdk task : {}", timer.getNowTime())
                , 0, 2);

        // 每一秒执行一次
        cronTask.doTask("*/1 * * * * *"
                , () -> cronLog.func("I am cron task : {}", timer.getNowTime()));
    }
}
