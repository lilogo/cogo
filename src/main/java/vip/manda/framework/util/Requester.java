package vip.manda.framework.util;

import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpRequest;
import vip.manda.framework.config.WebConfig;
import vip.manda.framework.core.Configs;

import java.util.Map;

/**
 * @author hongda.li 2022-04-22 11:15
 */
public class Requester {

    private static int timeout;
    private static int retry;

    public static void setTimeout(int timeout){
        Requester.timeout = timeout;
    }

    public static void setRetry(int retry){
        Requester.retry = retry;
    }

    public static String get(String url){
        return HttpRequest.get(url).execute().body();
    }

    public static String post(String url){
        return HttpRequest.post(url).execute().body();
    }

    public static String get(String url, Map<String, Object> params){
        HttpRequest request = HttpRequest.get(url).form(params).setConnectionTimeout(timeout);
        int temp = 0;
        while (temp <= retry){
            String result = request.execute().body();
            if (result != null){
                return result;
            }
        }
        return null;
    }

    public static String get(String url, String body){
        HttpRequest request = HttpRequest.get(url).body(body).setConnectionTimeout(timeout);
        int temp = 0;
        while (temp <= retry){
            String result = request.execute().body();
            if (result != null){
                return result;
            }
        }
        return null;
    }

    public static String get(String url, Map<String, Object> params, String body){
        HttpRequest request = HttpRequest.get(url).form(params).body(body).setConnectionTimeout(timeout);
        int temp = 0;
        while (temp <= retry){
            String result = request.execute().body();
            if (result != null){
                return result;
            }
        }
        return null;
    }

    public static String post(String url, Map<String, Object> params){
        HttpRequest request = HttpRequest.post(url).form(params).setConnectionTimeout(timeout);
        int temp = 0;
        while (temp <= retry){
            String result = request.execute().body();
            if (result != null){
                return result;
            }
        }
        return null;
    }

    public static String post(String url, String body){
        HttpRequest request = HttpRequest.post(url).body(body).setConnectionTimeout(timeout);
        int temp = 0;
        while (temp <= retry){
            String result = request.execute().body();
            if (result != null){
                return result;
            }
        }
        return null;
    }

    public static String post(String url, Map<String, Object> params, String body){
        HttpRequest request = HttpRequest.post(url).form(params).body(body).setConnectionTimeout(timeout);
        int temp = 0;
        while (temp <= retry){
            String result = request.execute().body();
            if (result != null){
                return result;
            }
        }
        return null;
    }

    /**
     * 获取请求uri，与上下文路径分离
     * @param uri 真实的请求uri
     * @return 分离后的uri
     */
    public static String getUri(String uri){
        String context = Configs.getStr(WebConfig.CONTEXT);
        return "".equals(context)
                ? uri
                : StrUtil.removePrefix(uri, context);
    }
}
