package vip.manda.framework.web.server;

/**
 * @author hongda.li 2022-04-21 15:15
 */
public abstract class WebServer {
    /**
     * 启动内嵌式Web服务器
     */
    public abstract void startServer();

    /**
     * 销毁内嵌式Web服务器
     */
    public abstract void stopServer();
}
