package vip.manda.test.util;

import cn.hutool.log.LogFactory;
import cn.hutool.log.StaticLog;
import cn.hutool.log.dialect.console.ConsoleColorLogFactory;

/**
 * @author hongda.li 2022-04-26 14:14
 */
public class StreamTest {
    public static void main(String[] args) {
        LogFactory.setCurrentLogFactory(ConsoleColorLogFactory.class);
        StaticLog.info("hello world");
    }
}
