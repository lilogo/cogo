package vip.manda.framework.data.repository;

import vip.manda.framework.log.Log;
import vip.manda.framework.log.LogFactory;

/**
 * @author hongda.li 2022-04-02 17:09
 */
public class MybatisLog implements org.apache.ibatis.logging.Log {
    private final Log log;

    public MybatisLog(String name){
        this.log = LogFactory.getLog(name);
    }

    @Override
    public boolean isDebugEnabled() {
        return true;
    }

    @Override
    public boolean isTraceEnabled() {
        return true;
    }

    @Override
    public void error(String s, Throwable throwable) {
        log.error(s, throwable);
    }

    @Override
    public void error(String s) {
        log.error(s);
    }

    @Override
    public void debug(String s) {
        log.info(s);
    }

    @Override
    public void trace(String s) {
        log.info(s);
    }

    @Override
    public void warn(String s) {
        log.debug(s);
    }
}
