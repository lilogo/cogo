package vip.manda.framework.task;

import cn.hutool.core.annotation.AnnotationUtil;
import cn.hutool.cron.CronUtil;
import vip.manda.framework.bean.BeanContainer;
import vip.manda.framework.bean.BeanFactory;
import vip.manda.framework.core.Application;
import vip.manda.framework.core.Configs;
import vip.manda.framework.core.Function;
import vip.manda.framework.log.Log;
import vip.manda.framework.log.LogFactory;

import java.util.List;
import java.util.Set;

/**
 * @author hongda.li 2022-04-21 15:39
 */
@Function(order = Function.Common.TASK_FUNCTION)
public class TaskFunction implements Function.FunctionMethod {

    private static final Log log = LogFactory.getLog();

    @Override
    public void doFunction(Set<Class<?>> classes) {
        if (!AnnotationUtil.hasAnnotation(Application.getMain(), EnableTask.class)){
            return;
        }
        log.func("execute function[{}]", TaskFunction.class.getName());
        Configs.setDefaultConfig(Configs.Task.THREAD_SIZE, Configs.Task.Value.THREAD_SIZE);
        BeanContainer container = BeanFactory.getContainer();
        classes.stream()
                .filter(clazz -> clazz == JdkTask.class || clazz == CronTask.class)
                .forEach(container::setBean);
        CronUtil.setMatchSecond(true);
        CronUtil.start();
    }
}
